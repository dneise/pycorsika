from distutils.core import setup, Extension

setup(
    name='py_mmcs_corsika',
    version='0.0.1',
    description='reads and writes MAGIC Monte Carlo Corsika Runs and Events',
    url='https://bitbucket.org/dneise/pycorsika',
    author='Dominik Neise, Sebastian Mueller',
    author_email='dneise@phys.ethz.ch, sebmuell@phys.ethz.ch',
    license='MIT',
    packages=[
        'py_mmcs_corsika',
        ],
    install_requires=[
        'numpy',
        'docopt',
    ],
    zip_safe=False
)