#!/usr/bin/python
# -*- coding: utf-8 -*-
""" Write MMCS cer-output files

Usage:
    MmcsWriter.py OUTFILE

"""
from __future__ import print_function
__all__ = ['Writer',]

import struct
import numpy as np
import sys
import math
import re
import os.path
import time
from docopt import docopt

class Writer(object):

    def __init__(self, path, write_run_header=True):
        """
        path : filepath to be written to.
        write_run_header : if True, the run_header will be written immediately.
                            default=True
        """
        filename = os.path.split(path)[1]
        if not re.match("cer[0,1,2,3,4,5,6,7,8,9]{6}", filename):
            print(" Error ".center(80, '-'))
            print("The Filename of an MMCS file is traditionally 'cer######'")
            print("I.e. 'cer' and then 6 digits, the runnumber, no file extension.")
            print("Your filename:", filename, "does not fit. This might lead to problems with")
            print("programs, that expect such a filename, like e.g. Ceres.")
            print(" Error ".center(80, '-'))
            print("For your security, and since we deduce the runnumber from the filename")
            print("We abort here")
            sys.exit(2)

        self._f = open(path, 'wb')

        # I forgot, why I start at 1. but I **guess** that I checked normal
        # MMCS files and looked at the first event.
        self.photon_id = 0

        # The MMCS Run Header consists of 273 floats. In order to make it easier
        # for researchers to play with these values without having the MMCS
        # manual at hand, we store them in dicts. The string keys were made to be
        # understandable. Mostly they are copies of comments from the MMCS manual.
        # So the keys are long strings.
        # Some of them made no sense for us, so we were not able to choose nice
        # keys. You are invited to improve the situation however.
        self.run_header_dict = {
            'AATM': np.array([-1.58060196e+02,  -3.08136044e+01,   3.45165849e-01,
                             -4.01833357e-04,   9.20443854e-04], dtype=np.float32),
            'BATM': np.array([1.21066016e+03,   1.20392957e+03,   1.36447937e+03,
                             7.97052002e+02,   1.00000000e+00], dtype=np.float32),
            'CATM': np.array([9.94186375e+05,   7.49333188e+05,   6.36143062e+05,
                             7.33384188e+05,   1.25051935e+10], dtype=np.float32),
            'CETA': np.array([0.3946, 0.72000003, 0.94959998,
                             -1.07000005,  2.06999993], dtype=np.float32),
            'CKA': np.array([0.00000000e+00,   1.00000001e-01,   0.00000000e+00,
                            0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
                            2.50000000e-01,   5.00000000e-01,   7.50000000e-01,
                            1.00000000e+00,   5.00000000e-01,   2.00000003e-01,
                            9.26406622e-01,   1.12407136e+00,   1.49600006e+02,
                            1.49600006e+02,   2.35531822e-01,   2.06000000e-01,
                            1.35000005e-01,   2.22000003e-01,   5.00000000e-01,
                            0.00000000e+00,   6.34299994e-01,   6.89499974e-01,
                            8.73700023e-01,   6.62400007e-01,   3.89499992e-01,
                            2.44320607e+00,   9.12400663e-01,   0.00000000e+00,
                            0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
                            0.00000000e+00,   1.00000000e+00,   1.00000000e+05,
                            0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
                            0.00000000e+00], dtype=np.float32),
            'CSTRBA': np.array([0., 0., 0., 0., 0.64819998,
                                0.51630002, 0., 0., 0., 0.67799997,
                                0.91399997], dtype=np.float32),
            'HLAY': np.array([0., 0., 0., 0., 0.], dtype=np.float32),
            'NFLAIN': 0.0,
            'NFLCHE + 100 x NFRAGM': 200.0,
            'NFLDIF': 0.0,
            'NFLPI0 + 100 x NFLPIF': 0.0,
            'X-displacement of inclined observation plane': 0.0,
            'Y-displacement of inclined observation plane': 0.0,
            'Z-displacement of inclined observation plane': 0.0,
            'date of begin run': 131010,
            'energy cutoff for photons in GeV': 0.02,
            'energy range': np.array([100., 50000.], dtype=np.float32),
            'flag for EGS4 treatment of em. component': 1.0,
            'flag for NKG treatment of em. component': 0.0,
            'kin. energy cutoff for electrons in GeV': 0.02,
            'kin. energy cutoff for hadrons in GeV': 0.30000001,
            'kin. energy cutoff for muons in GeV': 0.30000001,
            'observation levels': np.array([220000.], dtype=np.float32),
            'phyiscal constants': np.array([6.37131520e+08, 6.00000000e+05, 2.00000000e+06,
                                            -1.33003992e+03,  0.00000000e+00,   4.58059646e-02,
                                            5.73089600e-01,   5.28304204e-02,   2.50000000e+00,
                                            2.06999993e+00,   8.19999981e+00,   1.00000001e-01,
                                            0.00000000e+00,   0.00000000e+00,   1.00002337e+00,
                                            9.67266317e-03,   0.00000000e+00,   0.00000000e+00,
                                            0.00000000e+00,   0.00000000e+00,   3.77000008e+01,
                                            1.53287299e-04,   9.38641739e+00,   2.00000009e-03,
                                            2.99792466e+10,   1.00000000e+00,   5.40302277e-01,
                                            1.57000005e+00,   1.00000000e-15,   2.09999997e-02,
                                            0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
                                            2.00000000e+01,   0.00000000e+00,   0.00000000e+00,
                                            0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
                                            0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
                                            0.00000000e+00,   0.00000000e+00,   2.61909604e-01,
                                            8.99834216e-01,   0.00000000e+00,   1.03899205e+00,
                                            2.71383405e-01,   1.37035995e+02], dtype=np.float32),
            'run number': float(filename[3:]),
            'scatter range in x direction for Cherenkov': 0.0,
            'scatter range in y direction for Cherenkov': 0.0,
            'slope of energy spektrum': -2.7,
            'theta angle of normal vector of inclined observation plane': 0.0,
            'phi angle of normal vector of inclined observation plane': 0.0,
            'version of program': 6.5,
        }
        # The same we wrote about the Run Header is true for the Event Header as well.
        self.event_header_dict = {
            'CERENKOV Flag (is a bitmap --> usersguide)': '0x2c59',
            'CURVED flag (0=standard, 2=CURVED)': 2.0,
            'Cherenkov bandwidth in nm: (lower, upper) end': np.array([290., 900.], dtype=np.float32),
            'Cherenkov bunch size in the case of Cherenkov calculations': 1.0,
            'Cherenkov output directed to particle output file (= 0.) or Cherenkov output file (= 1.)': 1.0,
            'DPMJET cross-section flag (0.=no DPMJET, 1.=DPMJET)': 0.0,
            'DPMJET interaction flag (0.=no DPMJET, 1.=DPMJET)': 0.0,
            'EFRCTHN energy fraction of thinning level hadronic': 0.0,
            'EFRCTHN x THINRAT energy fraction of thinning level em-particles': 0.0,
            "Earth's magnetic field in uT: (x,z)": np.array([30.3, 24.1], dtype=np.float32),
            'NEUTRINO flag': 0.0,
            'NFLAIN': 0.0,
            'NFLCHE': 0.0,
            'NFLDIF': 0.0,
            'NFLPI0': 0.0,
            'NFLPIF': 0.0,
            'NFRAGM': 2.0,
            'NKG radial distribution range in cm': 20000.0,
            'QGSJET X-sect. flag (0.=no QGSJET, 1.=QGSJETOLD,2.=QGSJET01c, 3.=QGSJET-II)': 3.0,
            'QGSJET interact. flag (0.=no QGSJET, 1.=QGSJETOLD,2.=QGSJET01c, 3.=QGSJET-II)': 3.0,
            'SIBYLL cross-section flag (0.= no SIBYLL, 1.=vers.1.6; 2.=vers.2.1)': 0.0,
            'SIBYLL interaction flag (0.= no SIBYLL, 1.=vers.1.6; 2.=vers.2.1)': 0.0,
            'VENUS/NE X US/EPOS cross-section flag (0=neither, 1.=VENUSSIG,2./3.=NEXUSSIG, 4.=EPOSSIG)': 0.0,
            'actual weight limit WMAX for thinning hadronic': 0.0,
            'actual weight limit WMAX x WEITRAT for thinning em-particles': 0.0,
            'altitude (cm) of horizontal shower axis (skimming incidence)': 0.0,
            'angle (in rad) between array x-direction and magnetic north': -0.12217305,
            'angle in radian: (zenith, azimuth)': np.array([0., 0.], dtype=np.float32),
            'computer flag (3=UNIX, 4=Macintosh)': 3.0,
            'core location for scattered events in cm: (x,y)': np.array([[0.0, 0.0]], dtype=np.float32),
            'date of begin run (yymmdd)': int(time.strftime("%y%m%d")),
            'energy cutoff for photons in GeV': 0.02,
            'energy range': np.array([100., 50000.], dtype=np.float32),
            'event number': 1,
            'flag for activating EGS4': 1.0,
            'flag for activating NKG': 0.0,
            'flag for additional muon information on particle output file': 0.0,
            'flag for hadron origin of electromagnetic subshower on particle tape': 0.0,
            'flag for observation level curvature (CURVOUT) (0.=flat, 1.=curved)': 0.0,
            'flag indicating that explicite charm generation is switched on': 0.0,
            'grid spacing of Cherenkov detectors in cm (x, y) direction': np.array([1500., 1500.], dtype=np.float32),
            'high-energy hadr. model flag (0.=HDPM,1.=VENUS, 2.=SIBYLL,3.=QGSJET, 4.=DPMJET, 5.=NE X US, 6.=EPOS)': 3.0,
            'kin. energy cutoff for electrons in GeV': 0.02,
            'kin. energy cutoff for hadrons in GeV': 0.3,
            'kin. energy cutoff for muons in GeV': 0.3,
            'length of each Cherenkov detector in cm in (x, y) direction': np.array([100., 100.], dtype=np.float32),
            'low-energy hadr. model flag (1.=GHEISHA, 2.=UrQMD, 3.=FLUKA)': 3.0,
            'max. radius (in cm) for radial thinning': 0.0,
            'momentum in GeV/c in (x, y, -z) direction;': np.array([0., 0., np.pi], dtype=np.float32),
            'muon multiple scattering flag (1.=Moliere, 0.=Gauss)': 1.0,
            'number of Cherenkov detectors in (x, y) direction': np.array([27., 27.], dtype=np.float32),
            'number of first target if fixed': 0.0,
            'observation levels': np.array([220000.], dtype=np.float32),
            'particle id (particle code or A x 100 + Z for nuclei)': 1,
            'phi interval (in degree): (lower, upper edge) ': np.array([0.,  180.], dtype=np.float32),
            'theta interval (in degree): (lower, upper edge) ': np.array([0.,  180.], dtype=np.float32),
            'random number sequences: (seed, #calls, #billion calls)': np.array([[0., 0., 0.],
                                                                                 [0., 0., 0.],
                                                                                 [0., 0., 0.]], dtype=np.float32),
            'run number': 1.0,
            'skimming incidence flag (0.=standard, 1.=skimming)': 0.0,
            'slope of energy spektrum': 0.0,
            'starting altitude in g/cm2': 0.0,
            'starting height (cm)': 0.0,
            'step length factor for multiple scattering step length in EGS4': 1.0,
            'total energy in GeV': 1000.,
            'transition energy high-energy/low-energy model (in GeV)': 80.0,
            'version of program': 6.5,
            'viewing cone VIEWCONE (in deg): (inner, outer) angle': np.array([0., 0.], dtype=np.float32),
            'z coordinate (height) of first interaction in cm': 1800000.0,
        }

        self.__run_header_written = False
        if write_run_header:
            self.writeRunHeader()
            self.__run_header_written = True

    def writeRunHeader(self):
        """ writes Run Header based on self.run_header_dict contents
            Might be called already in __init__ unless
        """
        if self.__run_header_written:
            print("Run Header already written. Ignored.")
            return

        h = self.run_header_dict
        d = np.zeros(273, dtype=np.float32)
        d[0] = struct.unpack('f', 'RUNH')[0]
        d[1] = h['run number']
        d[2] = h['date of begin run']
        d[3] = h['version of program']
        d[4] = len(h['observation levels'])
        for i in range(10):
            try:
                d[5+i] = h['observation levels'][i]
            except IndexError:
                d[5+i] = 0.
        d[15] = h['slope of energy spektrum']
        d[16] = h['energy range'][0]
        d[17] = h['energy range'][1]
        d[18] = h['flag for EGS4 treatment of em. component']
        d[19] = h['flag for NKG treatment of em. component']
        d[20] = h['kin. energy cutoff for hadrons in GeV']
        d[21] = h['kin. energy cutoff for muons in GeV']
        d[22] = h['kin. energy cutoff for electrons in GeV']
        d[23] = h['energy cutoff for photons in GeV']
        d[24:74] = h['phyiscal constants']
        d[74] = h['X-displacement of inclined observation plane']
        d[75] = h['Y-displacement of inclined observation plane']
        d[76] = h['Z-displacement of inclined observation plane']
        d[77] = h['theta angle of normal vector of inclined observation plane']
        d[78] = h['phi angle of normal vector of inclined observation plane']
        # now some constants, I don't understand
        d[94:134] = h['CKA']
        d[134:139] = h['CETA']
        d[139:150] = h['CSTRBA']
        d[247] = h['XSCATT']
        d[248] = h['YSCATT']
        d[249:254] = h['HLAY']
        d[254:259] = h['AATM']
        d[259:264] = h['BATM']
        d[264:269] = h['CATM']
        d[269] = h['NFLAIN']
        d[270] = h['NFLDIF']
        d[271] = h['NFLPI0 + 100 x NFLPIF']
        d[272] = h['NFLCHE + 100 x NFRAGM']
        self._f.write(d.tostring())
        self.__run_header_written = True

    def _writeEventHeader(self, h):
        """ h: dict with the keys of self.event_header_dict
        """
        d = np.zeros(273, dtype=np.float32)
        d[0] = d[0] = struct.unpack('f', 'EVTH')[0]
        d[1] = h['event number']
        d[2] = h['particle id (particle code or A x 100 + Z for nuclei)']
        d[3] = h['total energy in GeV']
        d[4] = h['starting altitude in g/cm2']
        d[5] = h['number of first target if fixed']
        d[6] = h['z coordinate (height) of first interaction in cm']
        d[7:10] = h['momentum in GeV/c in (x, y, -z) direction;']
        d[10:12] = h['angle in radian: (zenith, azimuth)']

        d[12] = len(h['random number sequences: (seed, #calls, #billion calls)'])
        for i in range(10):
            try:
                d[13 + 3*i:13 + 3*(i+1)] = h['random number sequences: (seed, #calls, #billion calls)'][i]
            except IndexError:
                d[13 + 3*i:13 + 3*(i+1)] = 0.

        d[43] = self.run_header_dict['run number']
        d[44] = h['date of begin run (yymmdd)']
        d[45] = h['version of program']
        d[46] = len(h['observation levels'])
        for i in range(10):
            try:
                d[47:47 + i] = h['observation levels'][i]
            except IndexError:
                d[47:47+i] = 0.

        d[57] = h['slope of energy spektrum']
        d[58:60] = h['energy range']
        d[60] = h['kin. energy cutoff for hadrons in GeV']
        d[61] = h['kin. energy cutoff for muons in GeV']
        d[62] = h['kin. energy cutoff for electrons in GeV']
        d[63] = h['energy cutoff for photons in GeV']
        d[64] = h['NFLAIN']
        d[65] = h['NFLDIF']
        d[66] = h['NFLPI0']
        d[67] = h['NFLPIF']
        d[68] = h['NFLCHE']
        d[69] = h['NFRAGM']
        d[70:72] = h["Earth's magnetic field in uT: (x,z)"]
        d[72] = h['flag for activating EGS4']
        d[73] = h['flag for activating NKG']
        d[74] = h['low-energy hadr. model flag (1.=GHEISHA, 2.=UrQMD, 3.=FLUKA)']
        d[75] = h['high-energy hadr. model flag (0.=HDPM,1.=VENUS, 2.=SIBYLL,3.=QGSJET, 4.=DPMJET, 5.=NE X US, 6.=EPOS)']
        d[76] = int(h['CERENKOV Flag (is a bitmap --> usersguide)'], 16)  # <-- this is a hex string and we need to convert to int
        d[77] = h['NEUTRINO flag']
        d[78] = h['CURVED flag (0=standard, 2=CURVED)']
        d[79] = h['computer flag (3=UNIX, 4=Macintosh)']
        d[80:82] = h['theta interval (in degree): (lower, upper edge) ']
        d[82:84] = h['phi interval (in degree): (lower, upper edge) ']
        d[84] = h['Cherenkov bunch size in the case of Cherenkov calculations']
        d[85:87] = h['number of Cherenkov detectors in (x, y) direction']
        d[87:89] = h['grid spacing of Cherenkov detectors in cm (x, y) direction']
        d[89:91] = h['length of each Cherenkov detector in cm in (x, y) direction']
        d[91] = h['Cherenkov output directed to particle output file (= 0.) or Cherenkov output file (= 1.)']
        d[92] = h['angle (in rad) between array x-direction and magnetic north']
        d[93] = h['flag for additional muon information on particle output file']
        d[94] = h['step length factor for multiple scattering step length in EGS4']
        d[95:97] = h['Cherenkov bandwidth in nm: (lower, upper) end']
        d[97] = len(h['core location for scattered events in cm: (x,y)'])
        for i in range(20):
                try:
                    d[98+i] = h['core location for scattered events in cm: (x,y)'][i][0]
                    d[118+i] = h['core location for scattered events in cm: (x,y)'][i][1]
                except IndexError:
                    d[98+i] = 0.
                    d[118+i] = 0.
        d[138] = h['SIBYLL interaction flag (0.= no SIBYLL, 1.=vers.1.6; 2.=vers.2.1)']
        d[139] = h['SIBYLL cross-section flag (0.= no SIBYLL, 1.=vers.1.6; 2.=vers.2.1)']
        d[140] = h['QGSJET interact. flag (0.=no QGSJET, 1.=QGSJETOLD,2.=QGSJET01c, 3.=QGSJET-II)']
        d[141] = h['QGSJET X-sect. flag (0.=no QGSJET, 1.=QGSJETOLD,2.=QGSJET01c, 3.=QGSJET-II)']
        d[142] = h['DPMJET interaction flag (0.=no DPMJET, 1.=DPMJET)']
        d[143] = h['DPMJET cross-section flag (0.=no DPMJET, 1.=DPMJET)']
        d[144] = h['VENUS/NE X US/EPOS cross-section flag (0=neither, 1.=VENUSSIG,2./3.=NEXUSSIG, 4.=EPOSSIG)']
        d[145] = h['muon multiple scattering flag (1.=Moliere, 0.=Gauss)']
        d[146] = h['NKG radial distribution range in cm']
        d[147] = h['EFRCTHN energy fraction of thinning level hadronic']
        d[148] = h['EFRCTHN x THINRAT energy fraction of thinning level em-particles']
        d[149] = h['actual weight limit WMAX for thinning hadronic']
        d[150] = h['actual weight limit WMAX x WEITRAT for thinning em-particles']
        d[151] = h['max. radius (in cm) for radial thinning']
        d[152:154] = h['viewing cone VIEWCONE (in deg): (inner, outer) angle']
        d[154] = h['transition energy high-energy/low-energy model (in GeV)']
        d[155] = h['skimming incidence flag (0.=standard, 1.=skimming)']
        d[156] = h['altitude (cm) of horizontal shower axis (skimming incidence)']
        d[157] = h['starting height (cm)']
        d[158] = h['flag indicating that explicite charm generation is switched on']
        d[159] = h['flag for hadron origin of electromagnetic subshower on particle tape']
        d[167] = h['flag for observation level curvature (CURVOUT) (0.=flat, 1.=curved)']
        self._f.write(d.tostring())

    def _writeEventFooter(self, h):
        """
        h : dict with keys of self.event_header_dict
        """
        d = np.zeros(273, dtype=np.float32)
        d[0] = struct.unpack('f', 'EVTE')[0]
        d[1] = h['event number']
        d[2] = self.__last_event_N_photons
        self._f.write(d.tostring())

    def writeEvent(self, data_block=None):
        self._writeEventHeader(self.event_header_dict)
        self._writeDataBlock(data_block)
        self._writeEventFooter(self.event_header_dict)
        self.event_header_dict['event number'] += 1

    def _writeDataBlock(self, data_block):
        self.__last_event_N_photons = data_block.shape[0]
        self._f.write(data_block.tostring())
        size = self._f.tell()
        number_of_padding_bytes = ((size/1092)+1)*1092 - size
        self._f.write('\00'*number_of_padding_bytes)

    def __del__(self):
        self._writeRunFooter()
        self._f.close()

    def _writeRunFooter(self):
        d = np.zeros(273, dtype=np.float32)
        d[0] = struct.unpack('f', 'RUNE')[0]
        d[1] = self.run_header_dict['run number']
        d[2] = self.event_header_dict['event number']-1
        self._f.write(d.tostring())

        size = self._f.tell()
        #number_of_blocks = size/22932
        #full_size = (number_of_blocks+1)*22932
        self._f.write('\00'*((size/22932 + 1)*22932 - size))
        self._f.close()

if __name__ == '__main__':

    args = docopt(__doc__)

    writer = Writer(args['OUTFILE'])
    N_events = 1
    N_photons_per_event = 100000

    theta_max = 3.  # degree
    d_max = 1. - np.cos(np.deg2rad(theta_max))

    for _ in range(N_events):
        d = np.zeros((N_photons_per_event, 7), dtype=np.float32)
        # 0: encoded( wavelength, reuse_id, parent_particle_type)
        # 1: X position [cm]
        # 2: Y position [cm]
        # 3: cos of angle between photon direction and X-axis. Corsika-speak "cos(U)"
        # 4: cos of angle between photon direction and Y-axis. Corsika-speak "cos(V)"
        # 5: time since creation of this photon [ns].
        # 6: height of production above observation level [cm].

        wavelength = 480.
        reuse_id = 1.
        parent_particle_type = 2.

        d[:, 0] = wavelength + reuse_id*1000. + parent_particle_type*100000.
        d[:, 1] = np.random.uniform(-250, 250, N_photons_per_event)
        d[:, 2] = np.random.uniform(-250, 250, N_photons_per_event)
        phi = np.random.uniform(0., 2*np.pi, N_photons_per_event)
        cos_theta = np.random.uniform(np.cos(np.deg2rad(theta_max)), 1., N_photons_per_event)
        theta = np.arccos(cos_theta)

        d[:, 3] = np.sin(theta) * np.cos(phi)
        d[:, 4] = np.sin(theta) * np.sin(phi)
        d[:, 5] = 23333.
        d[:, 6] = 700000.

        writer.writeEvent(d)
    del writer
