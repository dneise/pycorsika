from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__all__ = ['EventGetter']

import struct
import os.path
import numpy as np
import gzip
import os


class ShowerEvent(object):
    Header = None
    PhotonData = None
    Footer = None

    def generate_telescope_events(self):
        set_of_reuse_ids = np.sort(np.unique(self.PhotonData['imov']))
        self.set_of_reuse_ids = set_of_reuse_ids
        for reuse_id in set_of_reuse_ids:
            yield TelescopeEvent(self, reuse_id)


class TelescopeEvent(object):

    @property
    def Core(self):
        return self.Header['core locations for reuse-events in cm: (x, y)'][self.ReuseId - 1]

    def __init__(self, mother, reuse_id):
        self.Header = mother.Header.copy()
        self.Footer = mother.Footer.copy()
        self.ReuseId = reuse_id
        self.Header["core location in cm: (x,y)"] = self.Core
        self.Header["reuse number"] = self.ReuseId

        photon_ids = np.where(mother.PhotonData['imov'] == reuse_id)[0]

        self.PhotonData = {}
        # if len(mother.set_of_reuse_ids) > 1:
        #    IPython.embed()

        self.PhotonData['pos'] = mother.PhotonData['pos'][photon_ids]
        self.PhotonData['pos'][:, 0:2] -= self.Core

        self.PhotonData['dir'] = mother.PhotonData['dir'][photon_ids]
        self.PhotonData['time'] = mother.PhotonData['time'][photon_ids]
        self.PhotonData['height'] = mother.PhotonData['height'][photon_ids]
        self.PhotonData['wavelength'] = mother.PhotonData[
            'wavelength'][photon_ids]
        self.PhotonData['parent'] = mother.PhotonData['parent'][photon_ids]


class SubBlockGetter(object):
    blockSize = 22932  # bytes
    subBlockSize = 1092  # bytes

    blockSize_words = blockSize // 4
    subBlockSize_words = subBlockSize // 4
    blockSize_subBlocks = blockSize // subBlockSize

    def __init__(self, filename):
        if '.gz' == os.path.splitext(filename)[1]:
            self._f = gzip.open(filename, 'rb')
        else:
            self._f = open(filename, 'rb')

        self.file_path = filename

        self._filesize = None
        self._number_of_sub_blocks = None

    @property
    def filesize(self):
        if self._filesize is None:
            file_stat = os.stat(self.file_path)
            if file_stat.st_size < 4 * 1024**3:
                with open(self.file_path, "rb") as raw_file:
                    raw_file.seek(-4, 2)
                    self._filesize = struct.unpack("i", raw_file.read(4))[0]    
            else:
                if not self._f.seekable():
                    raise Exception(
                        "file object on seekable, cannot determine filesize.")
                current_position = self._f.tell()
                self._filesize = self._f.seek(0, 2)
                self._f.seek(current_position, 0)
        
        return self._filesize

    def __len__(self):
        return self.filesize // self.subBlockSize

    def __getitem__(self, index):
        if not self._f.seekable():
            raise Exception("file object not seekable, cannot getitem.")

        if index >= 0:
            self._f.seek(self.subBlockSize * index, 0)
        else:
            self._f.seek(self.subBlockSize * index, 2)
        return np.frombuffer(
            self._f.read(self.subBlockSize),
            dtype=np.float32,
            count=self.subBlockSize_words)

    def __iter__(self):
        while True:
            buffer = self._f.read(self.subBlockSize)
            if(len(buffer) < self.subBlockSize):
                break

            block = np.frombuffer(buffer, dtype=np.float32,
                                  count=self.subBlockSize_words)
            yield block


class ShowerEventGetter(object):
    _marker = {
        'RUNH': struct.unpack('f', b'RUNH')[0],
        'EVTH': struct.unpack('f', b'EVTH')[0],
        'RUNE': struct.unpack('f', b'RUNE')[0],
        'EVTE': struct.unpack('f', b'EVTE')[0],
    }

    def __init__(self, filename):
        self.sub_blocks = SubBlockGetter(filename)
        self.RunFooter = self._assert_run_footer()
        self.RunHeader = self._assert_run_header()

    def _assert_run_header(self):
        """ We assume the first subblock is a run header, if not, we raise
        """
        if not self.sub_blocks[0][0] == self._marker['RUNH']:
            raise IOError(
                "can not find 'RUNH'-_marker at file start, maybe corrupted")
        return parse_mmcs_RunHeader(self.sub_blocks[0])

    def _assert_run_footer(self):
        """ We assume in some one of the last sub blocks there must be the run Footer

        If the first subblock (when searching backwards), that is not completely zero, 
        is not a run footer, we raise.
        """

        last_sub_blocks = [self.sub_blocks[sub_block_id]
                           for sub_block_id
                           in range(len(self.sub_blocks) - self.sub_blocks.blockSize_subBlocks, len(self.sub_blocks))
                           ]

        # walking backwards through the last few sub blocks
        for sb in last_sub_blocks[::-1]:
            if (sb == 0.).all():
                continue
            if not sb[0] == self._marker['RUNE']:
                raise IOError(
                    "last non emty subblock does not start with 'RUNE'")
            return sb

    def __iter__(self):
        data_block = []
        sub_block_iterator = iter(self.sub_blocks)

        event = ShowerEvent()
        states = [
            "expecting_event_header_or_run_end",
            "reading_body_expecting_event_end",
        ]
        state = "expecting_event_header_or_run_end"
        for subblock in sub_block_iterator:

            if state == "expecting_event_header_or_run_end":

                if not subblock[0] in [self._marker["EVTH"], self._marker["RUNE"]]:
                    raise IOError(
                        "expecting EVTH or RUNE, but not found, file corrupted?")
                elif subblock[0] == self._marker["EVTH"]:
                    event.Header = parse_full_mmcs_EventHeader(subblock)
                    state = "reading_body_expecting_event_end"
                elif subblock[0] == self._marker["RUNE"]:
                    raise StopIteration()

            elif state == "reading_body_expecting_event_end":

                if subblock[0] == self._marker["EVTE"]:
                    event.Footer = subblock
                    state = "expecting_event_header_or_run_end"
                    yield self._prepare(event, data_block)
                    data_block = []
                else:
                    data_block.append(subblock)

    def _prepare(self, event, data_block):
        if not data_block:
            event.PhotonData = {}
            event.PhotonData['data'] = np.zeros((0, 7), np.float32)
            event.PhotonData['parent'] = np.zeros((0,), np.int)
            event.PhotonData['imov'] = np.zeros((0,), np.int)
            event.PhotonData['time'] = np.zeros((0,), np.float32)
            event.PhotonData['wavelength'] = np.zeros((0,), np.float32)
            event.PhotonData['height'] = np.zeros((0,), np.float32)
            event.PhotonData['pos'] = np.zeros((0, 3), np.float32)
            event.PhotonData['dir'] = np.zeros((0, 3), np.float32)
            return event

        data_block = np.concatenate(data_block).reshape(-1, 7)

        # Unfortunately some lines in the data block can be zero
        # Those we have to remove, of course
        good_lines = np.where((data_block[:, :] != 0.).any(axis=1))[0]
        data = data_block[good_lines]

        event.PhotonData = {}
        event.PhotonData['data'] = data

        code = np.round(data[:, 0]).astype(int)
        j = code // 100000
        event.PhotonData['parent'] = j
        imov = (code - j * 100000) // 1000

        event.PhotonData['imov'] = imov
        wavelength = np.mod(data[:, 0], 1000.)
        event.PhotonData['wavelength'] = wavelength

        prod_time = data[:, 5]
        event.PhotonData['time'] = prod_time
        height = data[:, 6]
        event.PhotonData['height'] = height

        n = data.shape[0]
        pos = np.zeros((n, 3), dtype=np.float32)
        pos[:, :-1] = data[:, 1:3]
        event.PhotonData['pos'] = pos

        dir = np.zeros((n, 3), dtype=np.float32)
        dir[:, :-1] = data[:, 3:5]
        dir[:, -1] = np.sqrt((dir * dir).sum(axis=1) * -1. + 1.)
        event.PhotonData['dir'] = dir
        return event


class EventGetter(object):

    def __init__(self, filename):
        self.__shower_getter = ShowerEventGetter(filename)
        self.RunHeader = self.__shower_getter.RunHeader

    def __iter__(self):
        for shower in self.__shower_getter:
            for telescope_event in shower.generate_telescope_events():
                yield telescope_event

        self.RunFooter = self.__shower_getter.RunFooter

    def print_raw_mmcs_sub_block(self, block):
        out = 'MMCS raw sub block ' + str(block) + '\n'
        out += 'index (fortran style starts at 1), float value\n'
        idx = 1
        for value in self.__shower_getter.sub_blocks[block]:
            out += str(int(idx)) + '\t' + str(value) + '\n'
            idx += 1

        print(out)


def parse_mmcs_RunHeader(run_header):
    """ parse the run header of a corsika file into a dict

        **this function is called by parseByteStringToDict()** and a user normally 
        does not need to call this function directly.

        The run header of a corsika file is a 272 float list, 
        where each number has a meaning by its position in the list.
        This is documented in the Corsika Manual.
        this is just moving the plain 272-float numbers from the original
        header int a dict.
        Not sure, if the key naming is well done :-/
    """
    h = run_header
    d = {}
    d['run number'] = int(h[1])
    d['date of begin run'] = int(h[2])
    d['version of program'] = h[3]
    n_obs_levels = int(h[4])
    if (n_obs_levels < 1) or (n_obs_levels > 10):
        ValueException(
            'number of observation levels n must be 0 < n < 11, but is: ' + str(h[4]))
    d['observation levels'] = h[5:5 + n_obs_levels]
    d['slope of energy spektrum'] = h[15]
    d['energy range'] = h[16:18]
    d['flag for EGS4 treatment of em. component'] = h[18]
    d['flag for NKG treatment of em. component'] = h[19]
    d['kin. energy cutoff for hadrons in GeV'] = h[20]
    d['kin. energy cutoff for muons in GeV'] = h[21]
    d['kin. energy cutoff for electrons in GeV'] = h[22]
    d['energy cutoff for photons in GeV'] = h[23]
    d['phyiscal constants'] = h[24:74]
    d['X-displacement of inclined observation plane'] = h[74]
    d['Y-displacement of inclined observation plane'] = h[75]
    d['Z-displacement of inclined observation plane'] = h[76]
    d['theta angle of normal vector of inclined observation plane'] = h[77]
    d['phi angle of normal vector of inclined observation plane'] = h[78]
    # now some constants, I don't understand
    d['CKA'] = h[94:134]
    d['CETA'] = h[134:139]
    d['CSTRBA'] = h[139:150]
    d['XSCATT'] = h[247]
    d['YSCATT'] = h[248]
    d['HLAY'] = h[249:254]
    d['AATM'] = h[254:259]
    d['BATM'] = h[259:264]
    d['CATM'] = h[264:269]
    d['NFLAIN'] = h[269]
    d['NFLDIF'] = h[270]
    d['NFLPI0 + 100 x NFLPIF'] = h[271]
    d['NFLCHE + 100 x NFRAGM'] = h[272]
    return d


def parse_full_mmcs_EventHeader(event_header):
    """ parse the event header of a corsika file into a dict

        **this function is called by parseByteStringToDict()** and a user normally 
        does not need to call this function directly.

        The event header of a corsika file is a 272 float list, 
        where each number has a meaning by its position in the list.
        This is documented in the Corsika Manual.
        this is just moving the plain 272-float numbers from the original
        header int a dict.
        Not sure, if the key naming is well done :-/
    """
    h = event_header
    d = {}
    d['event number'] = int(h[1])
    d['particle id (particle code or A x 100 + Z for nuclei)'] = int(h[2])
    d['total energy in GeV'] = h[3]
    d['starting altitude in g/cm2'] = h[4]
    d['number of first target if fixed'] = h[5]
    d['z coordinate (height) of first interaction in cm'] = h[6]
    d['momentum in GeV/c in (x, y, -z) direction;'] = h[7:10]
    d['angle in radian: (zenith, azimuth)'] = h[10:12]
    n_random_number_sequences = int(h[12])
    if (n_random_number_sequences < 1) or (n_random_number_sequences > 10):
        ValueException(
            'number of random number sequences n must be 0 < n < 11, but is: ' + str(h[12]))
    seed_info = h[13:13 + 3 * n_random_number_sequences].reshape(n_random_number_sequences, -1)
    d['random number sequences: (seed, #calls, #billion calls)'] = seed_info
    d['run number'] = h[43]
    d['date of begin run (yymmdd)'] = int(h[44])
    d['version of program'] = h[45]
    n_obs_levels = int(h[46])
    if (n_obs_levels < 1) or (n_obs_levels > 10):
        ValueException(
            'number of observation levels n must be 0 < n < 11, but is: ' + str(h[46]))
    d['observation levels'] = h[47:47 + n_obs_levels].reshape(n_obs_levels, -1)
    d['slope of energy spektrum'] = h[57]
    d['energy range'] = h[58:60]
    d['kin. energy cutoff for hadrons in GeV'] = h[60]
    d['kin. energy cutoff for muons in GeV'] = h[61]
    d['kin. energy cutoff for electrons in GeV'] = h[62]
    d['energy cutoff for photons in GeV'] = h[63]
    d['NFLAIN'] = h[64]
    d['NFLDIF'] = h[65]
    d['NFLPI0'] = h[66]
    d['NFLPIF'] = h[67]
    d['NFLCHE'] = h[68]
    d['NFRAGM'] = h[69]
    d["Earth's magnetic field in uT: (x,z)"] = h[70:72]
    d['flag for activating EGS4'] = h[72]
    d['flag for activating NKG'] = h[73]
    d['low-energy hadr. model flag (1.=GHEISHA, 2.=UrQMD, 3.=FLUKA)'] = h[74]
    d['high-energy hadr. model flag (0.=HDPM,1.=VENUS, 2.=SIBYLL,3.=QGSJET, 4.=DPMJET, 5.=NE X US, 6.=EPOS)'] = h[75]
    d['CERENKOV Flag (is a bitmap --> usersguide)'] = hex(int(h[76]))
    d['NEUTRINO flag'] = h[77]
    d['CURVED flag (0=standard, 2=CURVED)'] = h[78]
    d['computer flag (3=UNIX, 4=Macintosh)'] = h[79]
    d['theta interval (in degree): (lower, upper edge) '] = h[80:82]
    d['phi interval (in degree): (lower, upper edge) '] = h[82:84]
    d['Cherenkov bunch size in the case of Cherenkov calculations'] = h[84]
    d['number of Cherenkov detectors in (x, y) direction'] = h[85:87]
    d['grid spacing of Cherenkov detectors in cm (x, y) direction'] = h[87:89]
    d['length of each Cherenkov detector in cm in (x, y) direction'] = h[89:91]
    d['Cherenkov output directed to particle output file (= 0.) or Cherenkov output file (= 1.)'] = h[91]
    d['angle (in rad) between array x-direction and magnetic north'] = h[92]
    d['flag for additional muon information on particle output file'] = h[93]
    d['step length factor for multiple scattering step length in EGS4'] = h[94]
    d['Cherenkov bandwidth in nm: (lower, upper) end'] = h[95:97]
    num_reuse = h[97]
    d['number i of uses of each Cherenkov event'] = num_reuse
    core_x = h[98:98 + num_reuse]
    core_y = h[118:118 + num_reuse]
    d['core locations for reuse-events in cm: (x, y)'] = np.vstack(
        (core_x, core_y)).transpose()
    d['SIBYLL interaction flag (0.= no SIBYLL, 1.=vers.1.6; 2.=vers.2.1)'] = h[138]
    d['SIBYLL cross-section flag (0.= no SIBYLL, 1.=vers.1.6; 2.=vers.2.1)'] = h[139]
    d['QGSJET interact. flag (0.=no QGSJET, 1.=QGSJETOLD,2.=QGSJET01c, 3.=QGSJET-II)'] = h[140]
    d['QGSJET X-sect. flag (0.=no QGSJET, 1.=QGSJETOLD,2.=QGSJET01c, 3.=QGSJET-II)'] = h[141]
    d['DPMJET interaction flag (0.=no DPMJET, 1.=DPMJET)'] = h[142]
    d['DPMJET cross-section flag (0.=no DPMJET, 1.=DPMJET)'] = h[143]
    d['VENUS/NE X US/EPOS cross-section flag (0=neither, 1.=VENUSSIG,2./3.=NEXUSSIG, 4.=EPOSSIG)'] = h[144]
    d['muon multiple scattering flag (1.=Moliere, 0.=Gauss)'] = h[145]
    d['NKG radial distribution range in cm'] = h[146]
    d['EFRCTHN energy fraction of thinning level hadronic'] = h[147]
    d['EFRCTHN x THINRAT energy fraction of thinning level em-particles'] = h[148]
    d['actual weight limit WMAX for thinning hadronic'] = h[149]
    d['actual weight limit WMAX x WEITRAT for thinning em-particles'] = h[150]
    d['max. radius (in cm) for radial thinning'] = h[151]
    d['viewing cone VIEWCONE (in deg): (inner, outer) angle'] = h[152:154]
    d['transition energy high-energy/low-energy model (in GeV)'] = h[154]
    d['skimming incidence flag (0.=standard, 1.=skimming)'] = h[155]
    d['altitude (cm) of horizontal shower axis (skimming incidence)'] = h[156]
    d['starting height (cm)'] = h[157]
    d['flag indicating that explicite charm generation is switched on'] = h[158]
    d['flag for hadron origin of electromagnetic subshower on particle tape'] = h[159]
    d['flag for observation level curvature (CURVOUT) (0.=flat, 1.=curved)'] = h[167]
    return d


if __name__ == '__main__':
    import sys
    eg = EventGetter(sys.argv[1])
